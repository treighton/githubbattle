import Axios from "axios";

const id = "client_id";
const secret = "secret";
const params = `?client_id=${id}&client_secret=${secret}`;

const fetchPopularRepos = language => {
  const encodedURI = window.encodeURI(
    `https://api.github.com/search/repositories?q=stars:>1+language:${language}&sort=stars&order=desc&type=Repositories`
  );

  return Axios.get(encodedURI).then(response => response.data.items);
};

const getProfile = username => {
  return Axios.get(`https://api.github.com/users/${username}`).then(user => {
    return user.data;
  });
};

const getRepos = username => {
  return Axios.get(`https://api.github.com/users/${username}/repos`).then(
    user => {
      return user.data;
    }
  );
};

const battle = players => {};

export { fetchPopularRepos };
