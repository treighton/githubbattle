import React from "react";
import { Link, NavLink } from "react-router-dom";

const Nav = props => {
  return (
    <ul className="nav">
      <li>
        <NavLink exact active="active" to="/">
          Home
        </NavLink>
      </li>

      <li>
        <NavLink active="active" to="/popular">
          Popular
        </NavLink>
      </li>
      <li>
        <NavLink active="active" to="/battle">
          Battle
        </NavLink>
      </li>
    </ul>
  );
};

export default Nav;
