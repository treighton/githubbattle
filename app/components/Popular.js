import React from "react";
import PropTypes from "prop-types";
import { fetchPopularRepos } from "../utils/api";

const SelectedLanguage = props => {
  const languages = ["All", "JavaScript", "Ruby", "Java", "CSS", "Python"];
  return (
    <div>
      <ul className="languages">
        {languages.map(language => {
          return (
            <li
              style={
                language === props.selectedLanguage
                  ? { color: "#d0021b" }
                  : null
              }
              key={language}
              onClick={props.onSelect.bind(null, language)}
            >
              {language}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

SelectedLanguage.prototypes = {
  selectedLanguage: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired
};

const RepoGrid = props => {
  return <ul className="popular-list">
      {props.repos.map((repo, index) => {
          return <li key={repo.name} className="popular-item">
              <div className="popular-rank">#{index + 1}</div>
              <ul className="space-list-items">
                <li>
                  <img className="avatar" src={repo.owner.avatar_url} alt={"Avatar for " + repo.owner.login} />
                </li>
                <li>
                  <a href={repo.html_url}>{repo.name}</a>
                </li>
                <li>@{repo.owner.login}</li>
                <li>{repo.stargazers_count} stars</li>
              </ul>
            </li>;
      })}
    </ul>;
};

RepoGrid.prototypes = {
    repos: PropTypes.array.isRequired
}

class Popular extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedLanguage: "All",
      repos: null
    };
    this.updateLanguage = this.updateLanguage.bind(this);
  }

  componentDidMount() {
    this.updateLanguage(this.state.selectedLanguage);
  }

  updateLanguage(language) {
    this.setState(() => {
      return {
        selectedLanguage: language,
        repos: null
      };
    });
    fetchPopularRepos(language).then(res => {
      this.setState(() => {
        return {
          repos: res
        };
      });
    });
  }

  render() {
    return (
      <div>
        <SelectedLanguage
          selectedLanguage={this.state.selectedLanguage}
          onSelect={this.updateLanguage}
        />
        {!this.state.repos 
            ? <p>Loading</p> 
            : <RepoGrid repos={this.state.repos} />
            }
        
      </div>
    );
  }
}

export default Popular;
